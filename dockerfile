FROM wordpress
RUN chmod 777 /var/www/html -R
ENTRYPOINT [ "docker-entrypoint.sh" ]
CMD [ "apache2-foreground" ]
