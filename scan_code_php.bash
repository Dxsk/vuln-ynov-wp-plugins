#!/bin/bash

target_folder="${1}"
words_target=(${2})
words_excludes=(${3})

mkdir -p results_scanner_code
rsc="./results_scanner_code"

for word in "${words_target[@]}"
do 
    grep -e "${word}" -R ${target_folder} --exclude=$(for i in ${words_excludes};do echo $i; done) -n > ${rsc}/${word}_found.out
    count_line=$(cat ${rsc}/${word}_found.out | wc -l)
    if [[ ${count_line} -le 0 ]]
        then
            rm ${rsc}/${word}_found.out;
        else
            echo "word : ${word} was found ${count_line}* / $(echo 'cat' ${rsc}/${word}_found.out) "
    fi
    
done